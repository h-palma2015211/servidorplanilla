var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var igss = require('./routes/api/igss.route');
var salarioOrdinario = require('./routes/api/salarioOrdinario.route')
var empleado = require('./routes/api/empleado.route');
var estado = require('./routes/api/estado.route');
var planilla = require('./routes/api/planilla.route');
var app = express();
var port = 3001;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(function(req, res, next){
res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, authorization, X-Requested-With, Content-Type, Accept");
res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
next();
})


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);
app.use('/', igss);
app.use('/', salarioOrdinario);
app.use('/', empleado);
app.use('/', estado);
app.use('/', planilla);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  //res.render('error');
  res.json(err);
  next();
});




app.listen(port, function() {
  console.log("El servidor esta corriendo puerto: " + port);
});	

module.exports = app;
