var database = require("../config/database.config");
var bonificacion = {};

bonificacion.select = function(callback) {
    if(database) {
        var consulta = 'SELECT * FROM  bonificacion';
        database.query(consulta, function(error, resultado){
            if(error) throw error;
            callback(resultado);
        });
    }
}

module.exports = bonificacion;