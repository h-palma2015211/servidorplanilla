var database = require("../config/database.config");
var empleado = {};

empleado.selectAll = function(callback) {
    if(database) {
        var consulta = 'CALL sp_Select()';
        database.query(consulta, function(error, resultados){
            if(error) throw error;
            callback(resultados[0])
        });
    }

}

empleado.selectEdi = function (idEmpleado, callback){
    if(database) {
        database.query('CALL sp_selectEmpleado(?)', idEmpleado, function(error, resultados){
            if(error) throw error;
            callback(resultados[0])
        })
    } 
}


empleado.historial1 = function(idEmpleado, callback) {
    if(database) {
        var consulta = 'call sp_historialBonificacion(?)';
        database.query(consulta, idEmpleado, function(error, resultados){
            if(error) throw error;
            callback(resultados[0]);
        });

    }
}

empleado.historial2 = function(idEmpleado, callback) {
    if(database) {
        var consulta = 'call sp_historialRetencion(?)';
        database.query(consulta, idEmpleado, function(error, resultados){
            if(error) throw error;
            callback(resultados[0])
        })
    }
}

empleado.insert = function(data, callback) {
    if(database) {
        database.query('CALL sp_insertEmpleado(?, ?, ?, ?, ?, ?, ?, ?, ?)',
        [data.nombre, data.apellido, data.fechaInicio, data.idEstado,
        data.bonificacion, data.retencion, data.fechaInactividad, data.fecha1,
        data.fecha2],
        function(error, resultados){
            if(error) throw error;
            callback({"Mensaje" : "se ingreso"});
        });
    }
}

empleado.delete = function(idEmpleado, callback) {
    if(database) {
        database.query('CALL sp_deleteEmpleado(?)', idEmpleado,
        function(error, resultados){
            if(error) throw error;
            callback({"mensaje":"Eliminado"});
        });
    }
}


empleado.update = function(data, callback) {
    if(database) {
        database.query('CALL sp_updateEmpleado(?, ?, ?, ?, ?, ?, ?, ? , ?)',
        [data.nombre, data.apellido, data.idEstado,
         data.bonificacion, data.retencion, data.fechaInactividad,
         data.idEmpleado, data.fecha1, data.fecha2], function(error, resultados){
            if(error) throw error;            
            callback(resultados);

         });

        
    }
}

empleado.select = function(idEmpleado, callback){
    if(database) {
        database.query('SELECT * FROM empleado WHERE idEmpleado = ?',
        idEmpleado, function(error, resultado){
            if(error) throw error;
            callback(resultado[0]);
        });
    }
}




module.exports = empleado;