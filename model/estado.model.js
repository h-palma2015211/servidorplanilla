var database = require("../config/database.config");
var estado = {};

estado.selectAll = function(callback) {
    if(database){
       var consulta = 'SELECT * FROM estado';
       database.query(consulta, function(error, resultado){
            if(error) throw error;
            callback(resultado);
       }); 
    }
}
module.exports = estado;