var database = require("../config/database.config");
var igss = {};

igss.selectAll = function(callback) {
    if(database) {
        var consulta = 'SELECT * FROM igss';
        database.query(consulta, function(error, resultado){
            if(error) throw error;
            callback(resultado);
        });
    }
}

igss.select = function(idIgss, callback) {
   if(database) {
       var consulta = 'SELECT * FROM igss WHERE idIgss = ?';
       database.query(consulta, idIgss,
       function(error, resultado) {
           if(error) throw error;
            callback(resultado[0]);
       });
   } 
}

igss.insert = function(data, callback) {
    if(database) {
        database.query('INSERT INTO igss(_year, cuota) VALUES(?,?)',
        [data._year, data.cuota],
        function(error, resultado){
             if(error) throw error;
            callback({"affectedRows": resultado.affectedRows});
        });
    }
}


igss.update = function(data, callback) {
    if(database) {
        database.query('UPDATE igss SET _year =?,   cuota =? WHERE idIgss = ?', 
        [data._year, data.cuota, data.idIgss], function(error, resultado){
            if(error) throw error;
            callback(resultado);
        })
    }
}

igss.delete = function(idIgss, callback) {
    if(database) {
        database.query('DELETE FROM igss WHERE idIgss = ?', idIgss,
        function(error, resultado){
            if(error) throw error;
            callback({"mensaje": "Eliminado"});
        });
    }
}

 
module.exports = igss;