    var database = require("../config/database.config");
var planilla = {};


planilla.buscar = function(data, callback) {
    if(database) {
        database.query('CALL  sp_selectPlanilla(?, ?)',
        [data.mes, data._year], function(error, resultados){
            if(error) throw error;
            callback(resultados[0]);
        });
    }
}

planilla.obtener = function(data, callback) {
    if(database) {
        database.query('CALL sp_obtenerDatos(?, ?)', [data.mes, data._year],
        function(error, resultados){
            if(error) throw error;
            callback(resultados[0]);
        });
    }
}

planilla.historial = function(idPlanilla, callback) {
    if(database){
        database.query('CALL sp_selectMostrarPlanilla(?)', idPlanilla,
        function(error, resultados){
            if(error) throw error;
            callback(resultados[0]);
        })
    }
}


planilla.guardarPla = function(data, callback) {
    if(database){
        database.query('INSERT INTO planilla(_year, mes, fechaGeneracion) VALUES(?,  ?, NOW())',
        [data._year, data.mes], function(error, resultados){
            if(error) throw error;
            callback({"Mensaje" : "se ingreso"});
        });
    }
}

planilla.guardarDetalle = function(data, callback) {
    if(database) {
        database.query('CALL sp_guardarPlanillaDetalle(?, ?, ?, ?, ?, ?, ?)',
        [data.totalSalario, data.sueldoLiquido, data.salarioOrdinario, data.igss, data.idEmpleado,
        data.bonificacion, data.retencion], function(error, resultados) {
            if(error) throw error;
            callback({"Mensaje" : "se ingreso"});
        });
    }
}


planilla.borrar = function(idPlanilla, callback) {
    if(database) {
        database.query('CALL sp_elminarPlanilla(?)', idPlanilla,
        function(error, resultados) {
             if(error) throw error;
            callback({"Mensaje" : "se elimino"});
        });
    }
} 

planilla.detalles = function(idPlanilla, callback) {
    if(database) {
        database.query('CALL sp_detsPlanilla(?)', idPlanilla,
        function(error, resultados){
            if(error) throw error;
            callback(resultados[0]);
        })
    }
}

planilla.editarDetalle = function(data, callback) {
    if(database) {
        database.query('CALL sp_editarPlanilla(?, ?, ?, ?, ?)',
        [data.sueldoOrdinario, data.retencion, data.bonificacion,
         data.igss, data.idDetallePlanilla],
        function(error, resultados) {
            if(error) throw error;
            callback({"Mensaje" : "se edito"});
         });
    } 
}

planilla.getDetalle = function (idDetallePlanilla, callback) {
    if(database) {
        database.query('SELECT * FROM detallePlanilla WHERE idDetallePlanilla = ?',
        idDetallePlanilla, function(error, resultados) {
             if(error) throw error;
            callback(resultados[0]);
        });
    }
} 

module.exports = planilla;