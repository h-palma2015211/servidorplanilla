var database = require("../config/database.config");
var salarioOrdinario = {};

salarioOrdinario.selectAll = function(callback) {
    if(database) {
        var consulta = 'SELECT * FROM salarioOrdinario';
        database.query(consulta, function(error, resultado){
            if(error) throw error;
            callback(resultado);
        });
    }
}

salarioOrdinario.select = function(idSalarioOrdinario, callback) {
    if(database) {
        database.query('SELECT * FROM salarioOrdinario WHERE idSalarioOrdinario = ?',
        idSalarioOrdinario, function(error, resultado){
            if(error) throw error;
            callback(resultado[0]);
        });
    }
}

salarioOrdinario.insert = function(data, callback) {
    if(database) {
        database.query('INSERT INTO salarioOrdinario(_year, salario) VALUES(?,?)', 
        [data._year, data.salario],
        function(error, resultado){
            if(error) throw error;
            callback({"affectedRows": resultado.affectedRows});
        });
    }
}


salarioOrdinario.update = function(data, callback) {
    if(database) {
        database.query('UPDATE salarioOrdinario set _year = ?, salario = ? WHERE idSalarioOrdinario = ?',
        [data._year, data.salario, data.idSalarioOrdinario],
        function(error, resultado) {
            if(error) throw error;
            callback(resultado);
        });
    }
}
salarioOrdinario.delete = function(idSalarioOrdinario, callback) {
    if(database) {
        database.query('DELETE FROM salarioOrdinario WHERE idSalarioOrdinario = ?', idSalarioOrdinario,
        function(error, resultado){
            if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
        });
    }
}

module.exports = salarioOrdinario;