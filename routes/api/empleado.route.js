var express = require('express');
var router = express.Router();
var empleado = require('../../model/empleado.model');


router.get('/empleado/', function(req, res, next){
    empleado.selectAll(function(resultados) {
         if(typeof resultados !== 'undefined') {
            res.json(resultados);
        } else {
            res.json({"mensaje" : "no hay salario ordinario"});
        }
    })
})

router.get('/empleado/historial1/:idEmpleado', function(req, res, next){
    var idEmpleado = req.params.idEmpleado;
    empleado.historial1(idEmpleado, function(resultados){
         if(typeof resultados !== 'undefined') {
            res.json(resultados);

        } else {
             res.json({"mensaje" : "no hay"});
        }
    })    
})

router.get('/empleado/historial2/:idEmpleado', function(req, res, next){
    var idEmpleado = req.params.idEmpleado;
    empleado.historial2(idEmpleado, function(resultados){
         if(typeof resultados !== 'undefined') {
            res.json(resultados);

        } else {
             res.json({"mensaje" : "no hay"});
        }
    }) 
})

router.get('/empleado/:idEmpleado', function(req, res, next){
    var idEmpleado = req.params.idEmpleado;

    empleado.selectEdi(idEmpleado, function(resultados){
         if(typeof resultados !== 'undefined') {
            res.json(resultados[0]);

        } else {
             res.json({"mensaje" : "no hay"});
        }
    });

})

router.post('/empleado', function(req, res, next){
    data = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        fechaInicio: req.body.fechaInicio,
        idEstado: req.body.idEstado,
        bonificacion: req.body.bonificacion,
        retencion: req.body.retencion,
        fechaInactividad: req.body.fechaInactividad,
        fecha1: req.body.fecha1,
        fecha2: req.body.fecha2
    }
    
    empleado.insert(data, function(resultado){
        if(resultado && resultado.affectedRows > 0) {
             res.json({"mensaje": "se ingreso!!"})
       } else {
            res.json({"mensaje": "no se ingreso"})
       }
    });
});

router.put('/empleado/:idEmpleado', function(req, res, next){
    var idEmpleado = req.params.idEmpleado;
    var data = {
        nombre: req.body.nombre,
        apellido: req.body.apellido,
        idEstado: req.body.idEstado,
        retencion: req.body.retencion,
        bonificacion: req.body.bonificacion,
        fechaInactividad: req.body.fechaInactividad,
        idEmpleado: idEmpleado,
        fecha1: req.body.fecha1,
        fecha2: req.body.fecha2
    }

    empleado.update(data, function(resultado){
        if(resultado.lenght > 0) {
            res.json({"mensaje": "se modifico"})
        } else {
            res.json({"mensaje": "No se modifico"})
        }
    });
})

router.delete('/empleado/:idEmpleado', function(req, res, next){
    var idEmpleado = req.params.idEmpleado;
    
    empleado.delete(idEmpleado, function(resultado){
         if(resultado && resultado.mensaje === "Eliminado") {
             res.json({"mensaje":"Se elimino el salario correctamente"});
        } else {
            res.json({"mensaje":"No se elimino la salario"});
        }
    })
})



module.exports = router;