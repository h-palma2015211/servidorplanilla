var express = require('express');
var router = express.Router();
var estado = require('../../model/estado.model');

router.get('/estado/', function(req, res, next){
    estado.selectAll(function(resultados) {
         if(typeof resultados !== 'undefined') {
            res.json(resultados[0]);
        } else {
            res.json({"mensaje" : "no hay salario ordinario"});
        }
    })
})

module.exports = router;