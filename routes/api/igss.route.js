var express = require('express');
var igss = require ('../../model/igss.model');
var router = express.Router();

router.get('/igss/', function(req, res, next){
    igss.selectAll(function(resultados){
        if(typeof resultados !== 'undefined') {
            res.json(resultados);
        } else {
            res.json({"mensaje" : "no hay igss"});
        }
    });
});

router.get('/igss/:idIgss', function(req, res, next) {
    var idIgss = req.params.idIgss;
    igss.select(idIgss, function(resultados) {
        if(typeof resultados !== 'undefined') {
            res.json(resultados);

        } else {
             res.json({"mensaje" : "no hay"});
        }
    })
});


router.post('/igss', function(req, res, next) {
    var data = {
        idIgss: null,
        _year: req.body._year,
        cuota: req.body.cuota
    }
    igss.insert(data, function(resultado) {
       if(resultado && resultado.affectedRows > 0) {
             res.json({"mensaje": "se ingreso!!"})
       } else {
            res.json({"mensaje": "no se ingreso"})
       }
    })
});


router.put('/igss/:idIgss', function(req, res, next){
    var idIgss = req.params.idIgss;
    var data = {
        _year: req.body._year,
        cuota: req.body.cuota,
        idIgss: idIgss
    }

    igss.update(data, function(resultado){
        if(resultado.lenght > 0) {
            res.json({"mensaje": "se modifico"})
        } else {
            res.json({"mensaje": "No se modifico"})
        }
    })
});

router.delete('/igss/:idIgss', function(req, res, next){

    var id = req.params.idIgss;

    igss.delete(id,  function(resultado) {
        if(resultado && resultado.mensaje === "Eliminado") {
      res.json({"mensaje":"Se elimino el igss correctamente"});
    } else {
      res.json({"mensaje":"No se elimino la igss"});
    }
    })
})

module.exports = router;