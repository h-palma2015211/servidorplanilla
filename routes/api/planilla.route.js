var express = require('express');
var planilla = require('../../model/planilla.model');
var router = express.Router();

router.post('/planillabuscar', function(req, res, next){
    var data = {
        mes: req.body.mes,
        _year: req.body._year
    }

    planilla.buscar(data, function(resultados){
        
        res.json(resultados);
    });
});

router.post('/planillaObtener', function(req, res, next){
    var data = {
        mes: req.body.mes,
        _year: req.body._year
    }
    planilla.obtener(data, function(resultados){
        res.json(resultados);
    });
});

router.post('/plDetalleAgregar', function(req, res, next) {
    var data = {
        totalSalario: req.body.totalSalario,
        sueldoLiquido: req.body.sueldoLiquido,
        salarioOrdinario: req.body.salarioOrdinario,
        igss: req.body.igss,
        idEmpleado: req.body.idEmpleado,
        bonificacion: req.body.bonificacion,
        retencion: req.body.retencion
    }

    planilla.guardarDetalle(data, function(resultados){
        res.json(resultados);
    });


});

router.post('/planillaAgregar', function(req, res, next) {
    var data = {
        _year: req.body._year,
        mes: req.body.mes
    }
    planilla.guardarPla(data, function(resultados) {
         res.json(resultados);
    });
});

router.get('/planilla/:idPlanilla', function(req, res, next){
    var idPlanilla = req.params.idPlanilla;

    planilla.historial(idPlanilla, function(resultados) {
        res.json(resultados);
    });
})

router.delete('/planilla/:idPlanilla', function(req, res, next) {
    var idPlanilla = req.params.idPlanilla;
    planilla.borrar(idPlanilla, function(resultados) {
        res.json(resultados);
    })
})

router.get('/detalles/:idPlanilla', function(req, res, next) {
    var idPlanilla = req.params.idPlanilla;

    planilla.detalles(idPlanilla, function(resultados) {
        res.json(resultados);
    })
}); 
router.get('/detalle/:idDetallePlanilla', function(req, res, next) {
    var idDetallePlanilla = req.params.idDetallePlanilla;
    planilla.getDetalle(idDetallePlanilla, function(resultados) {
        res.json(resultados);
    })
})

router.put('/detalles/:idDetallePlanilla', function(req, res, next) {
    var idDetallePlanilla = req.params.idDetallePlanilla;
    
    var data = {
        sueldoOrdinario: req.body.sueldoOrdinario,
        retencion: req.body.retencion,
        bonificacion: req.body.bonificacion,
        igss: req.body.igss,
        idDetallePlanilla: idDetallePlanilla
    }
    console.log(data);

    planilla.editarDetalle(data, function(resultados) {
        res.json(resultados);

    })

})



module.exports = router;