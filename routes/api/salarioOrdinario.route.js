var express = require('express');
var salarioOrdinario = require ('../../model/salarioOrdinario.model');
var router = express.Router();


router.get('/salarioOrdinario/', function(req, res, next){
    salarioOrdinario.selectAll(function(resultados) {
         if(typeof resultados !== 'undefined') {
            res.json(resultados);
        } else {
            res.json({"mensaje" : "no hay salario ordinario"});
        }
    })
})

router.get('/salarioOrdinario/:idSalarioOrdinario', function(req, res, next) {
    var idSalarioOrdinario = req.params.idSalarioOrdinario;
    salarioOrdinario.select(idSalarioOrdinario, function(resultados){
        if(typeof resultados !== 'undefined') {
            res.json(resultados);

        } else {
             res.json({"mensaje" : "no hay"});
        }
    })
})


router.post('/salarioOrdinario', function(req, res, next){
    var data = {
        idSalarioOrdinario: null,
        _year: req.body._year,
        salario: req.body.salario
    }

    salarioOrdinario.insert(data, function(resultado){
        if(resultado && resultado.afectedRows > 0) {
               res.json({"mensaje": "se ingreso!!"})
       } else {
            res.json({"mensaje": "no se ingreso"})
        }
    })
})

router.put('/salarioOrdinario/:idSalarioOrdinario', function(req, res, next) {
    var idSalarioOrdinario = req.params.idSalarioOrdinario;
    var data = {
        _year: req.body._year,
        salario: req.body.salario,
        idSalarioOrdinario: idSalarioOrdinario
    }

    salarioOrdinario.update(data, function(resultado) {
        if(resultado.lenght > 0) {
            res.json({"mensaje": "se modifico"})
        } else {
            res.json({"mensaje": "No se modifico"})
        }
    });
})

router.delete('/salarioOrdinario/:idSalarioOrdinario', function(req, res, next){
    var idSalarioOrdinario = req.params.idSalarioOrdinario;

    salarioOrdinario.delete(idSalarioOrdinario, function(resultado){
         if(resultado && resultado.mensaje === "Eliminado") {
            res.json({"mensaje":"Se elimino el salario correctamente"});
          } else {
             res.json({"mensaje":"No se elimino la salario"});
        }
    })
})




module.exports = router;
