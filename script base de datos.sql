CREATE DATABASE proyectoPlanilla;

use proyectoPlanilla;
CREATE TABLE salarioOrdinario(
	idSalarioOrdinario INT AUTO_INCREMENT NOT NULL,
	_year DATETIME  NOT NULL,
	salario DOUBLE NOT NULL,
	PRIMARY KEY (idSalarioOrdinario)

);

CREATE TABLE igss(
	idIgss INT AUTO_INCREMENT NOT NULL,
	_year DATETIME NOT NULL,
	cuota DOUBLE NOT NULL,
	PRIMARY KEY  (idIgss)
);

CREATE TABLE estado(
	idEstado INT AUTO_INCREMENT NOT NULL,
	nombre varchar(60),
	PRIMARY KEY(idEstado)
);

CREATE TABLE empleado(
	idEmpleado INT AUTO_INCREMENT NOT NULL,
	nombre varchar(60) NOT NULL,
	apellido varchar(60) NOT NULL,
	fechaInicio DATETIME NOT NULL,
	idEstado int NOT NULL, 
	fechaInactividad DATETIME NOT NULL,
	PRIMARY KEY (idEmpleado),
	FOREIGN KEY (idEstado) REFERENCES estado(idEstado)
);

CREATE TABLE planilla(
	idPlanilla INT AUTO_INCREMENT NOT NULL,
	_year DATETIME NOT NULL,
	fechaGeneracion DATETIME NOT NULL,
	mes DATETIME NOT NULL,
	PRIMARY KEY (idPlanilla)

);

CREATE TABLE detallePlanilla(
	idDetallePlanilla INT AUTO_INCREMENT NOT NULL,
	totalSalario DOUBLE NOT NULL,
	sueldoLiquido DOUBLE NOT NULL,
	salarioOrdinario DOUBLE NOT NULL,
	igss DOUBLE NOT NULL,
	idEmpleado INT NOT NULL,
	nombre varchar(60),
	idPlanilla INT,
	bonificacion INT,
	retencion INT,
	PRIMARY KEY (idDetallePlanilla),
	FOREIGN KEY(idPlanilla) REFERENCES planilla(idPlanilla)
);

CREATE TABLE bonificacion (
	idBonificacion INT AUTO_INCREMENT NOT NULL,
	cuota DOUBLE NOT NULL,
	fecha DATETIME NOT NULL,
	idEmpleado INT NOT NULL,
	PRIMARY KEY (idBonificacion),
	FOREIGN KEY (idEmpleado) references empleado(idEmpleado) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE retencion (
	idRetencion INT AUTO_INCREMENT NOT NULL,
	cuota DOUBLE NOT NULL,
	fecha DATETIME NOT NULL,
	idEmpleado INT NOT NULL,
	PRIMARY KEY (idRetencion),
	FOREIGN KEY (idEmpleado) references empleado(idEmpleado) ON DELETE CASCADE ON UPDATE CASCADE
);

